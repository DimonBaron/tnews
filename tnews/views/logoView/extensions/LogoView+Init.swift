//
//  LogoView+Init.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 28.07.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import UIKit

/**
    Extension perform creation of subviews
 
 */
extension LogoView {
    
    /**
        Method create all subviews
     
     */
    func configureView() {
        self.createCenterView()
        self.createLeftWingView()
        self.createRightWingView()
        self.createCrownView()
        self.createBottomView()
    }
    
    /**
        Methods below creating subviews
     */
    func createCenterView() {
        let image = LogoKit.imageOfShieldView
        
        /* create view */
        let view = UIImageView(image: image)
        view.setFrameOrigin(x: 37.0, y: 19.0)
        
        /* append to scene */
        self.addSubview(view)
        
        /* store view */
        self.centerView = view
    }
    
    func createLeftWingView() {
        let image = LogoKit.imageOfLeftWind
        
        /* create view */
        let view = UIImageView(image: image)
        view.setFrameOrigin(x: 2.0, y: 22.0)
        
        /* append to scene */
        self.addSubview(view)
        
        /* store view */
        self.leftWindView = view
    }
    
    func createRightWingView() {
        let image = LogoKit.imageOfRightWind
        
        /* create view */
        let view = UIImageView(image: image)
        view.setFrameOrigin(x: 70.0, y: 22.0)
        
        /* append to scene */
        self.addSubview(view)
        
        /* store view */
        self.rightWindView = view
    }
    
    func createCrownView() {
        let image = LogoKit.imageOfCrownView
        
        /* create view */
        let view = UIImageView(image: image)
        view.setFrame(origin: self.crownDefaultOrigin())
        
        /* append to scene */
        self.addSubview(view)
        
        /* store view */
        self.crownView = view
    }
    
    func createBottomView() {
        let image = LogoKit.imageOfBottomView
        
        /* create view */
        let view = UIImageView(image: image)
        view.setFrame(origin: self.bottomDefaultOrigin())
        
        /* append to scene */
        self.addSubview(view)
        
        /* store view */
        self.bottomView = view
    }
    
    func crownDefaultOrigin() -> CGPoint {
        return CGPoint(x: 48.2, y: 9.0)
    }
    
    func bottomDefaultOrigin() -> CGPoint {
        return CGPoint(x: 38.0, y: 97.0)
    }
    
}
