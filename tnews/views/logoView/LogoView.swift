//
//  LogoView.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 28.07.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import UIKit

/**
    Animatable view that represents logo of application
 
 */

class LogoView: UIView {
    
    var centerView:UIImageView?
    var leftWindView:UIImageView?
    var rightWindView:UIImageView?
    var crownView:UIImageView?
    var bottomView:UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.configureView()
    }
    
    public func startAnimation(completion:@escaping (Void)->(Void)) {
        self.resetAnimation()
        
        self.performStepOneAnimation { [weak self] in
            self?.performStepTwoAnimation(finish: { [weak self] (Void) -> (Void) in
                self?.finishAnimation {
                    completion()
                }
            })
        }
    }
    
    public func finishAnimation(completion:(Void)->(Void)) {
        completion()
    }
    
    public func performStepOneAnimation(finish:@escaping (Void) -> (Void)) {
        let view = self.centerView
        
        UIView.animate(withDuration: 2.0, animations: {
            view?.alpha = 1.0
        }, completion: { (finished) in
            finish()
        })
        
    }
    
    public func performStepTwoAnimation(finish:@escaping (Void) -> (Void)) {
        
        let crownView = self.crownView
        let bottomView = self.bottomView
        let crownOrigin = self.crownDefaultOrigin()
        let bottomOrigin = self.bottomDefaultOrigin()
        
        let leftWind = self.leftWindView
        let rightWind = self.rightWindView
        
        UIView.animate(withDuration: 3.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options: [], animations: {
            
            crownView?.setFrame(origin: crownOrigin)
            crownView?.alpha = 1.0
            
            bottomView?.setFrame(origin: bottomOrigin)
            bottomView?.alpha = 1.0
            
            leftWind?.alpha = 1.0
            leftWind?.transform = CGAffineTransform.identity
            rightWind?.alpha = 1.0
            rightWind?.transform = CGAffineTransform.identity
            
        }, completion: { (finished) in
            finish()
        })
        
    }
    
    

}

extension LogoView {
    
    public func resetAnimation() {
        self.resetWinds()
        self.resetShield()
        self.resetBottom()
        self.resetCrown()
    }
    
    private func resetCrown() {
        var origin = self.crownDefaultOrigin()
        origin.y -= 5.0
        let view = self.crownView
        
        view?.setFrame(origin: origin)
        view?.alpha = 0.0
    }
    
    private func resetWinds() {
        var leftTransform = CGAffineTransform.init(translationX: 18.0, y: -28.0)
        leftTransform = leftTransform.rotated(by: CGFloat( -Double.pi / 48.0))
        leftTransform = leftTransform.translatedBy(x: -18.0, y: 28.0)
        
        let leftWindView = self.leftWindView
        // leftWindView?.transform = leftTransform
        leftWindView?.alpha = 0.0
        
        var rightTransform = CGAffineTransform.init(translationX: -18.0, y: -28.0)
        rightTransform = rightTransform.rotated(by: CGFloat( Double.pi / 48.0))
        rightTransform = rightTransform.translatedBy(x: 18.0, y: 28.0)
        
        let rightWindView = self.rightWindView
        // rightWindView?.transform = rightTransform
        rightWindView?.alpha = 0.0
    }
    
    private func resetShield() {
        let view = self.centerView
        view?.alpha = 0.0
    }
    
    private func resetBottom() {
        var origin = self.bottomDefaultOrigin()
        origin.y += 5.0
        let view = self.bottomView
        
        view?.setFrame(origin: origin)
        view?.alpha = 0.0
    }
    
}
