//
//  CoreDataService.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 06.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import CoreData

class CoreDataService {
    
    static let sharedInstance = CoreDataService()
    
    var mainContext:NSManagedObjectContext!
    
    /**
        Setup and start up
     */
    class func setup(model:String, completionClosure: @escaping () -> ()) {
        // Try to locate local path to model
        guard let modelURL = Bundle.main.url(forResource: model, withExtension:"momd") else {
            fatalError("Error loading model from bundle")
        }
        
        // Try to model creation
        guard let mom = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Error initializing mom from: \(modelURL)")
        }
        
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        let service = CoreDataService.sharedInstance
        
        service.mainContext = NSManagedObjectContext(concurrencyType:.mainQueueConcurrencyType)
        service.mainContext.persistentStoreCoordinator = psc
        
        let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
        queue.async {
            guard let docURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last else {
                fatalError("Unable to resolve document directory")
            }
            let storeURL = docURL.appendingPathComponent("\(model).sqlite")
            do {
                try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
                //The callback block is expected to complete the User Interface and therefore should be presented back on the main queue so that the user interface does not need to be concerned with which queue this call is coming from.
                DispatchQueue.main.sync(execute: completionClosure)
            } catch {
                fatalError("Error migrating store: \(error)")
            }
        }
    }
    
    /**
        Perform saving via provieded block
     */
    func save(with block: @escaping (NSManagedObjectContext) -> (), competion: @escaping (Error?) -> ()) {
        guard let mainContext = self.mainContext else {
            fatalError("Try to save without main context")
        }
        
        let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        context.parent = mainContext
        
        context.perform {
            
            block(context)
            var getError:Error? = nil
            
            do {
                try context.save()
                mainContext.performAndWait {
                    do {
                        try mainContext.save()
                    } catch {
                        getError = error
                        // fatalError("Failure to save context: \(error)")
                    }
                }
            } catch {
                getError = error
                // fatalError("Failure to save context: \(error)")
            }
            
            DispatchQueue.main.async {
                competion( getError )
            }
        }
        
    }
    
    
    
}
