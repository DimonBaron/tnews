//
//  NSManagedObject+CoreDataService.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 06.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import CoreData

protocol CoreDataServiceImport {
    
    static var jsonMap:[String: String] { get }
    static var primaryKey:String { get }
    
    func didImport(_ dict:Dictionary<String, Any>)
    
}

extension NSManagedObject {
    
    class func importObject<T:NSManagedObject>(json:[String: Any], in context:NSManagedObjectContext) -> T? {
        guard let castSelf=self as? CoreDataServiceImport.Type else {
            // doesn't match
            return nil
        }
        
        let jsonMap = castSelf.jsonMap
        let primaryKey = castSelf.primaryKey
        
        guard let jsonPrimaryKey = jsonMap[primaryKey] else {
            return nil
        }
        
        /* try to find dot */
        var jsonPrimaryValue:Any!
        let paths = jsonPrimaryKey.components(separatedBy: ".")
        if (paths.count == 1) {
            jsonPrimaryValue = json[jsonPrimaryKey]
            if jsonPrimaryValue == nil {
                return nil
            }
        } else if (paths.count == 2) {
            guard let firstJson = json[ paths[0] ] as? Dictionary<String, Any> else {
                return nil
            }
            
            jsonPrimaryValue = firstJson[ paths[1] ]
            if jsonPrimaryValue == nil {
                return nil
            }
            
        } else {
            return nil
        }
        
        let className = NSStringFromClass(self).components(separatedBy: ".").last!
        let fetchRequest = NSFetchRequest<T>(entityName: className)
        
        let predicate = NSPredicate(format: "\(primaryKey) = %@", jsonPrimaryValue as! CVarArg)
        fetchRequest.predicate = predicate
        
        var fetchedObject:T? = nil
        do {
            let fetched = try context.fetch(fetchRequest)
            if (fetched.count > 0) {
                fetchedObject = fetched[0]
            }
        } catch {
            fatalError("Failed to fetch employees: \(error)")
        }
        
        if (fetchedObject == nil) {
            fetchedObject = NSEntityDescription.insertNewObject(forEntityName: className, into: context) as? T
        }
        
        /* mapped */
//        for (key, jsonKey) in jsonMap {
//            let type = fetchedObject?.typeByPropery(key)
//            
//            switch type {
//            case .integer16AttributeType:
//                
//                
//                break
//            case .integer64AttributeType:
//                
//                
//                break
//            default:
//                
//            }
//            
//            let value = json[jsonKey]
//            
//            fetchedObject?.setValue(value, forKey: key)
//        }
        if let fetchedObjectCast = fetchedObject as? CoreDataServiceImport {
            fetchedObjectCast.didImport( json )
        }
        
        return fetchedObject
    }
    
    func typeByPropery(_ nameOfProperty:String) -> NSAttributeType {
        for (name, attr) in  self.entity.attributesByName {
            let attrType = attr.attributeType
            
            if (name == nameOfProperty) {
                return attrType
            }
        }
        return .undefinedAttributeType
    }
    
    class func findAll<T:NSManagedObject>(in context:NSManagedObjectContext) -> [T] {
        return self.findAll(predicate: nil, sortDescriptors: [], in: context)
    }
    
    class func findAll<T:NSManagedObject>(predicate:NSPredicate?, in context:NSManagedObjectContext) -> [T] {
        
        return self.findAll(predicate: predicate, sortDescriptors: [], in: context)
    }
    
    class func findAll<T:NSManagedObject>(predicate:NSPredicate?, sortDescriptors:[NSSortDescriptor], in context:NSManagedObjectContext) -> [T] {
        
        let className = NSStringFromClass(self).components(separatedBy: ".").last!
        let fetchRequest = NSFetchRequest<T>(entityName: className)
        if (predicate != nil) {
            fetchRequest.predicate = predicate!
        }
        if (sortDescriptors.count > 0) {
            fetchRequest.sortDescriptors = sortDescriptors
        }
        
        var fetched:[T] = []
        do {
            fetched = try context.fetch(fetchRequest)
        } catch {
            fatalError("Failed to fetch employees: \(error)")
        }
        
        return fetched
    }
    
    class func findOne<T:NSManagedObject>(primary itemID:Int, in context:NSManagedObjectContext) -> T? {
        guard let castSelf=self as? CoreDataServiceImport.Type else {
            // doesn't match
            return nil
        }
        
        let primaryKey = castSelf.primaryKey
        
        let className = NSStringFromClass(self).components(separatedBy: ".").last!
        let fetchRequest = NSFetchRequest<T>(entityName: className)
        let predicate = NSPredicate.init(format: "%K = %@", primaryKey, NSNumber.init(value: itemID) )
        fetchRequest.predicate = predicate
        
        var fetched:[T] = []
        do {
            fetched = try context.fetch(fetchRequest)
        } catch {
            fatalError("Failed to fetch employees: \(error)")
        }
        
        if (fetched.count > 0) {
            return fetched[0]
        }
        
        return nil
    }
    
}
