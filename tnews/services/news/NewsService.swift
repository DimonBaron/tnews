//
//  NewsService.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 05.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation

protocol NewsServiceProtocol:class {
    
    func loadNewsList(_ completion:@escaping (Bool) -> ())
    func loadNewsContent(newsID:Int, completion:@escaping (Bool) -> ())
    
}

class NewsService:NewsServiceProtocol {
    
    /**
        Shared instance usefull for quick service access
     */
    static let sharedInstance = NewsService()
    
    /**
        Network helper
     */
    let newsHelper:NetworkService<TinkoffApiProvider>
    
    /**
        Init service
     */
    init() {
        self.newsHelper = NetworkService<TinkoffApiProvider>()
    }
    
    /**
        Perform loading news from net
     */
    public func loadNewsList(_ completion:@escaping (Bool) -> ()) {
        
        self.newsHelper.request(.newsList) { (result) -> (Void) in
            /* response payload */
            var rawItems:[Dictionary<String, Any>]?
            
            /* response */
            switch result {
            case .Success(let response):
                if response is [Dictionary<String, Any>] {
                    rawItems = response as? [Dictionary<String, Any>]
                }
                
                break
            case  .Error(let error):
                print("News list load error: \(error)")
                
                break
            }
            
            let completionLoading = completion
            
            /* cache data */
            if (rawItems != nil) {
                CoreDataService.sharedInstance.save(with: { (context) in
                    for rawItem in rawItems! {
                        
                        let item = NewsFeed.importObject(json: rawItem, in: context) as? NewsFeed
                        if (item != nil) {
                            // do somethink
                        }
                    }
                }, competion: { (error) in
                    let isSuccess = (error == nil) ? true : false
                    completionLoading( isSuccess )
                })
            }
        }
    }
    
    /**
     Perform loading news from net
     */
    public func loadNewsContent(newsID:Int, completion:@escaping (Bool) -> ()) {
        
        self.newsHelper.request(.loadNews(id: newsID)) { (result) -> (Void) in
            /* response payload */
            var rawItem:Dictionary<String, Any>?
            
            /* response */
            switch result {
            case .Success(let response):
                if response is Dictionary<String, Any> {
                    rawItem = response as? Dictionary<String, Any>
                }
                
                break
            case  .Error(let error):
                print("News list load error: \(error)")
                
                break
            }
            
            let completionLoading = completion
            
            /* cache data */
            if (rawItem != nil) {
                CoreDataService.sharedInstance.save(with: { (context) in
                    let item = NewsContent.importObject(json: rawItem!, in: context) as? NewsContent
                    if (item != nil) {
                        // do somethink
                    }
                }, competion: { (error) in
                    let isSuccess = (error == nil) ? true : false
                    completionLoading( isSuccess )
                })
            }
        }
    }
    
}
