//
//  NetworkService.swift
//  tnews
//
//  Created by Dmitry Avvakumov on 01.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation

public enum NetworkServiceMethod {
    case Get
    case Post
}

public enum NetworkServiceCompletion {
    case Success(response:Any)
    case Error(error:NSError)
}

public protocol NetworkServiceType {
    
    /// The target's base `URL`.
    var baseURL: URL { get }
    
    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String { get }
    
    /// The HTTP method used in the request.
    var method: NetworkServiceMethod { get }
    
    /// The parameters to be encoded in the request.
    var parameters: [String: Any]? { get }
    
    // The headers to be used in the request.
    var headers: [String: String]? { get }
    
    // Response payload
    var responsePayloadKey: String { get }
    
}

class NetworkService<T: NetworkServiceType> {
    
    func request(_ target:T, completion:@escaping (NetworkServiceCompletion) -> (Void)) {
        
        // Create the URLSession on the default configuration
        let defaultSessionConfiguration = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultSessionConfiguration)
        
        // Setup the request with URL
        let url = computeUrl(target)
        var urlRequest = URLRequest(url: url)
        
        // Payload data
        let responsePayloadKey = target.responsePayloadKey
        
        switch target.method {
        case .Get:
            urlRequest.httpMethod = "GET"
            
            break
        case .Post:
            // Convert POST string parameters to data using UTF8 Encoding
            let postParams = "api_key=APIKEY&email=example@example.com&password=password"
            let postData = postParams.data(using: .utf8)
            
            // Set the httpMethod and assign httpBody
            urlRequest.httpMethod = "POST"
            urlRequest.httpBody = postData
            
            break
        }
        
        // Create dataTask
        let dataTask = defaultSession.dataTask(with: urlRequest) { (data, response, error) in
            // result
            var json:Any? = nil
            var payload:Any? = nil
            var responseError:Error? = nil
            let result:NetworkServiceCompletion
            
            if (error != nil) {
                responseError = error!
            } else {
                do {
                    json = try JSONSerialization.jsonObject(with: data!)
                } catch {
                    let info = [ NSLocalizedDescriptionKey: "Not parsable response" ]
                    responseError = NSError(domain: "self.application", code: -1, userInfo: info)
                }
            }
            
            /* try to parse payload */
            if (json != nil) {
                if json is Dictionary<String, Any> {
                    let jsonDict = json as! Dictionary<String, Any>
                    payload = jsonDict[responsePayloadKey]
                }
                
                if (payload == nil) {
                    let info = [ NSLocalizedDescriptionKey: "Json not included `\(responsePayloadKey)` key" ]
                    responseError = NSError(domain: "self.application", code: -1, userInfo: info)
                }
            }
            
            /* Forming result of request */
            if (payload != nil) {
                result = NetworkServiceCompletion.Success(response: payload!)
            } else {
                if (responseError == nil) {
                    let info = [ NSLocalizedDescriptionKey: "Not parsable response" ]
                    responseError = NSError(domain: "self.application", code: -1, userInfo: info)
                }
                
                result = NetworkServiceCompletion.Error(error: responseError! as NSError)
            }
            
            DispatchQueue.main.async {
                completion( result );
            }
        }
        
        // Fire the request
        dataTask.resume()
    }
    
    private func computeUrl(_ target:T) -> URL {
        
        let baseURL = target.baseURL.absoluteString + "/" + target.path
        // let path = target.path
        
        // baseURL.appendPathComponent(path)
        let url = URL.init(string: baseURL)!
        
        return url
    }
    
}
