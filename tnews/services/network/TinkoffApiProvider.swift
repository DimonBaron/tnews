//
//  TinkoffApiProvider.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 05.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation

enum TinkoffApiProvider {
    case newsList
    case loadNews(id:Int)
}

extension TinkoffApiProvider:NetworkServiceType {
    
    var baseURL: URL { return URL.init(string: "https://api.tinkoff.ru/v1")! }
    
    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String {
        switch self {
        case .newsList:
            return "news"
        case .loadNews(let id):
            return "news_content?id=\(id)"
        }
    }
    
    /// The HTTP method used in the request.
    var method: NetworkServiceMethod {
        return .Get
    }
    
    /// The parameters to be encoded in the request.
    var parameters: [String : Any]? {
        return nil
    }
    
    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
    
    var responsePayloadKey: String {
        return "payload"
    }

    
}

