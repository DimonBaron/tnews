//
//  StartupViewController.swift
//  tnews
//
//  Created by Dmitry Avvakumov on 28.07.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import UIKit

class StartupViewController: UIViewController {
    
    /* Outlets */
    var logoView:LogoView! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.logoView.startAnimation {
            self.continueAction()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func continueAction() {
        AppRouter.sharedInstance.setStartup(showed: true, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
