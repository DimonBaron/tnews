//
//  StartupViewController+LayoutSubviews.swift
//  tnews
//
//  Created by Dmitry Avvakumov on 28.07.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import UIKit

/**
 Extension for manual manipulating of view hierarchy of StartupViewController
 
 */

extension StartupViewController {
    
    /** Perform layout */
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.layoutSubviews_layoutLogo()
    }
    
    func layoutSubviews_layoutLogo() {
        guard let view = self.logoView else {
            return
        }
        
        /* update frames */
        view.alignToCenterSuperview()
    }
    
    
}
