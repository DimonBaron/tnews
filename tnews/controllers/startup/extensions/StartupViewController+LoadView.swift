//
//  StartupViewController+LoadView.swift
//  tnews
//
//  Created by Dmitry Avvakumov on 28.07.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import UIKit

/**
 Extension for creation of view hierarchy of StartupViewController
 
 */

extension StartupViewController {
    
    /** Perform view loading */
    override func loadView() {
        let frame = CGRect.init(x: 0, y: 0, width: 320, height: 480)
        let view = UIView.init(frame: frame)
        view.backgroundColor = UIColor.white
        self.view = view
        
        self.loadView_createLogoView()
    }
    
    /** Create and customize logo */
    func loadView_createLogoView() {
        /* hierarchy */
        guard let rootView = self.view else {
            return
        }
        
        let frame = CGRect(x: 0.0, y: 0.0, width: 120.0, height: 120.0)
        let view = LogoView.init(frame: frame)
        view.backgroundColor = UIColor.clear
        
        /* add to scene */
        rootView.addSubview(view)
        
        /* store */
        self.logoView = view
    }
    
}
