//
//  NewsShowAssembly.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 07.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import UIKit

class NewsShowAssembly {
    
    class func assembly(moduleInput: NewsShowViewModel.ModuleInput) -> UIViewController {
        
        /* controller */
        let viewController = self.createViewController()
        
        /* viewModel */
        let dp = createDependecies()
        let viewModel = NewsShowViewModel(dependencies: dp)
        viewModel.moduleInput = moduleInput
        
        /* assambly */
        viewController.viewModel = viewModel
        viewModel.viewController = viewController
        
        return viewController
    }
    
    /**
     Create view controller
     */
    private class func createViewController() -> NewsShowViewController {
        return NewsShowViewController()
    }
    
    /**
     Create and configure dependency
     */
    private class func createDependecies() -> NewsShowViewModel.InputDependencies {
        let dp = NewsShowViewModel.InputDependencies.init(newsHelper: NewsService.sharedInstance)
        return dp
    }
    
}
