//
//  NewsShowViewController.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 07.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import UIKit

protocol NewsShowViewControllerProtocol: class {
    
    func display(html:String)
    func setNavigation(title:String)
    
}

class NewsShowViewController: UIViewController, NewsShowViewControllerProtocol {
    
    var viewModel:NewsShowViewModelProtocol?
    
    /* Outlets */
    @IBOutlet var scrollView:UIScrollView!
    @IBOutlet var titleLabel:UILabel!
    @IBOutlet var webView:UIWebView!
    
    /* Constraints */
    @IBOutlet var webViewHeightConstraint:NSLayoutConstraint!
    
    init() {
        super.init(nibName: "NewsShowViewController", bundle: nil)
    }
    
    required init(coder:NSCoder) {
        super.init(coder: coder)!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureUI()
        self.startObservingHeight()
        
        self.viewModel?.viewIsReady()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        self.stopObservingHeight()
    }
    
    
    // MARK: NewsListViewControllerInput
    
    func setNavigation(title: String) {
        self.navigationItem.title = title
    }
    
    func display(html:String) {
        
        self.webView.loadHTMLString(html, baseURL: nil)
    }
    
    // MARK:- Height observable
    
    func startObservingHeight() {
        let webView = self.webView
        webView!.scrollView.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    func stopObservingHeight() {
        let webView = self.webView
        webView!.scrollView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if (keyPath != "contentSize") {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        } else {
            self.updateWebViewSize()
        }
    }
    
    // MARK:- Web view
    
    func updateWebViewSize() {
        let height = self.webView.sizeThatFits(.zero).height
        
        self.webViewHeightConstraint.constant = height
    }

}
