//
//  NewsShowViewController+Configure.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 07.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import UIKit

extension NewsShowViewController: UIWebViewDelegate {
    
    func configureUI() {
        self.webView!.backgroundColor = UIColor.clear
        self.webView!.delegate = self
    }
    
    // MARK:- UIWebViewDelegate
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if (navigationType == .linkClicked) {
            let url = request.url
            UIApplication.shared.openURL(url!)
            
            return false
        }
        
        return true
    }
    
}
