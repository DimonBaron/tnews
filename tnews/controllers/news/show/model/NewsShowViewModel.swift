//
//  NewsShowViewModel.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 07.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation

protocol NewsShowViewModelProtocol {
    
    func viewIsReady()
    
}

class NewsShowViewModel: NewsShowViewModelProtocol {
    
    weak var viewController:NewsShowViewControllerProtocol?
    
    struct ModuleInput {
        let newsID:Int
    }
    
    struct InputDependencies {
        let newsHelper:NewsServiceProtocol
    }
    
    // Mark:- Dependencies
    var dp:InputDependencies
    
    var moduleInput:NewsShowViewModel.ModuleInput?
    
    init(dependencies:InputDependencies) {
        self.dp = dependencies
    }
    
    func viewIsReady() {
        guard let itemID = self.moduleInput?.newsID else {
            return
        }
        
        self.viewController?.setNavigation(title: "Новость")
        self.viewController?.display(html: self.prepareHTML())
        
        self.dp.newsHelper.loadNewsContent(newsID: itemID) { [weak self] (isSuccess) in
            self?.viewController?.display(html: (self?.prepareHTML())!)
        }
    }
    
    func prepareHTML() -> String {
        guard let itemID = self.moduleInput?.newsID else {
            return ""
        }
        
        let item = NewsFeed.findOne(primary: itemID, in: CoreDataService.sharedInstance.mainContext) as NewsFeed?
        
        var html = ""
        
        let cssPath = Bundle.main.path(forResource: "main.css", ofType: nil)
        var cssString = ""
        
        do {
            try cssString = String(contentsOfFile: cssPath!)
        } catch {
            print("Wrong css file path")
        }
        
        html += "<head><style>\n"
        html += cssString
        html += "</style></head>"
        
        html += "<body>"
        
        if let text = item?.content?.fullText {
            html += text
        }
        
        html += "</body>"
        
        
        return html
    }
    
}
