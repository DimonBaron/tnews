//
//  NewsListRouter.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 07.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import UIKit

protocol NewsListRouterProtocol {
    
    /**
        Open single news by specify ID
     */
    func openNews(_ newsID:NewsFeedPKType)
    
}

/**
    Module routing
 */
class NewsListRouter: NewsListRouterProtocol {
    
    // module
    var viewController:UIViewController?
    
    func openNews(_ newsID: Int) {
        let input = NewsShowViewModel.ModuleInput.init(newsID: newsID)
        let controller = NewsShowAssembly.assembly(moduleInput: input)
        
        self.viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
}
