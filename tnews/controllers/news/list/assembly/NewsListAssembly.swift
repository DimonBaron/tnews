//
//  NewsListAssembly.swift
//  tnews
//
//  Created by Dmitry Avvakumov on 01.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import UIKit

class NewsListAssembly {
    
    class func assembly() -> UIViewController {
        
        /* controller */
        let viewController = self.createViewController()
        
        /* viewModel */
        let dp = createDependecies()
        let viewModel = NewsListViewModel(dependencies: dp)
        
        /* router */
        let router = NewsListRouter()
        
        /* assambly */
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.viewController = viewController
        router.viewController = viewController
        
        return viewController
    }
    
    /**
        Create view controller
     */
    private class func createViewController() -> NewsListViewController {
        return NewsListViewController()
    }
    
    /**
        Create and configure dependency
     */
    private class func createDependecies() -> NewsListViewModel.InputDependencies {
        let dp = NewsListViewModel.InputDependencies.init(newsHelper: NewsService.sharedInstance)
        
        return dp
    }
    
}
