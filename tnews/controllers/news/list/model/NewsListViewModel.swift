//
//  NewsListViewModel.swift
//  tnews
//
//  Created by Dmitry Avvakumov on 01.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol NewsListModelInput: class {
    
    func viewIsReady()
    func startDownloading()
    
}

class NewsListViewModel: NSObject, NewsListModelInput, NSFetchedResultsControllerDelegate {
    
    weak var viewController:NewsListViewControllerInput?
    var fetchedController:NSFetchedResultsController<NewsFeed>?
    
    var isDownloading:Bool! = false
    
    struct InputDependencies {
        let newsHelper:NewsServiceProtocol
    }
    
    // Mark:- Dependencies
    var dp:InputDependencies
    
    init(dependencies:InputDependencies) {
        self.dp = dependencies
    }
    
    func viewIsReady() {
        guard let viewController = self.viewController else {
            return
        }
        
        /* title of controller */
        viewController.setNavigation(title: "Список новостей")
        
        /* setup fetch */
        self.setupFetchRequestController()
        do {
            try self.fetchedController?.performFetch()
        } catch {
            print("Error fetching: \(error)")
        }
        
        /* perform */
        self.fetchCachedDataAndSendToView()
        
        /* start to download latest news */
        self.performDownloadActualFeed()
    }
    
    func startDownloading() {
        /* start to download latest news */
        self.performDownloadActualFeed()
    }
    
    /**
        Method fetched data from cache and send collection to view controller
     */
    func fetchCachedDataAndSendToView() {
        /* models */
        var dataItems:[NewsListTableViewModel] = []
        
        let context = CoreDataService.sharedInstance.mainContext!
        let sorts = NSSortDescriptor.init(key: "publishDate", ascending: false)
        let coreDataItems = NewsFeed.findAll(predicate: nil, sortDescriptors: [sorts], in: context) as! [NewsFeed]
        if (coreDataItems.count > 0) {
            dataItems = NewsListTableViewModel.modelsFromCoreData(items: coreDataItems)
        }
        
        self.viewController?.setDataItems(dataItems)
    }
    
    /**
        Method performs downloading data
     */
    func performDownloadActualFeed() {
        /* check for current operation */
        if (self.isDownloading) { return }
        
        self.isDownloading = true
        
        self.viewController?.setDownloadProcess(true, animated: false)
        self.dp.newsHelper.loadNewsList { [weak self] (isSuccess) in
            print("Download finished")
            
            self?.viewController?.setDownloadProcess(false, animated: true)
            self?.isDownloading = false
        }
    }
    
    // MARK:- NSFetchedResultsController
    /**
        Method create and setup NSFetchedResultsController
     
     */
    func setupFetchRequestController() {
        /* create fetch */
        let fetchRequest = NSFetchRequest<NewsFeed>.init(entityName: "NewsFeed")
        let sortDescriptor = NSSortDescriptor.init(key: "publishDate", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        let context = CoreDataService.sharedInstance.mainContext
        let fetchController = NSFetchedResultsController.init(fetchRequest: fetchRequest, managedObjectContext: context!, sectionNameKeyPath: nil, cacheName: nil)
        
        /* assign delegates */
        fetchController.delegate = self
        
        /* store it */
        self.fetchedController = fetchController
    }
    
    // MARK:- NSFetchedResultsControllerDelegate
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.fetchCachedDataAndSendToView()
    }
    
}

/**
    Extension below uses for debug issue only
 */
extension NewsListViewModel {
    
    func dummyDataItems() -> [NewsListTableModelProtocol] {
        let model1 = NewsListTableViewModel.init()
        model1.title = "Setters and Getters apply to computed properties; such properties do not have storage in the instance - the value from the getter is meant to be computed from other instance properties. In your case, there is no x to be assigned."
        model1.dateString = "12 июня 2017"
        
        let model2 = NewsListTableViewModel.init()
        model2.title = "Новость и ничего больше"
        model2.dateString = "4 июня 2017"
        
        return [model1, model2]
    }
    
}
