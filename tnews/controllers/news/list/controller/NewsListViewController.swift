//
//  NewsListViewController.swift
//  tnews
//
//  Created by Dmitry Avvakumov on 01.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import UIKit

/**
    Protocol for public controller functions
 
 */
protocol NewsListViewControllerInput: class {
    
    func setNavigation(title:String)
    func setDataItems(_ items:[NewsListTableModelProtocol] )
    
    func setDownloadProcess(_ isDownloading: Bool, animated:Bool)
    
}

class NewsListViewController: UIViewController, NewsListViewControllerInput {

    var viewModel: NewsListModelInput?
    var router: NewsListRouter?
    
    /* table data */
    var dataItems:[NewsListTableModelProtocol]?
    
    /* Outlets */
    var tableView:UITableView!
    var activityIndicator:UIActivityIndicatorView!
    var pullToRefreshIndicator:UIActivityIndicatorView!
    
    /* Appereance */
    var isDownloading:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        
        viewModel?.viewIsReady()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: NewsListViewControllerInput
    func setNavigation(title: String) {
        self.navigationItem.title = title
    }
    
    func setDataItems(_ items: [NewsListTableModelProtocol]) {
        self.dataItems = items
        
        self.tableView?.reloadData()
        self.updateViewAppereance(animated: true)
    }
    
    func setDownloadProcess(_ isDownloading: Bool, animated: Bool) {
        self.isDownloading = isDownloading
        self.updateViewAppereance(animated: animated)
    }
    
    /**
        Method will update view controller appereance in order to current state:
         - Set or not data items
         - Downloading or not process
     */
    func updateViewAppereance(animated:Bool) {
        /* input data */
        let isDownloading = self.isDownloading
        let isEmpty = (self.dataItems != nil && (self.dataItems?.count)! > 0) ? false : true
        
        /* params */
        let tableAlpha = CGFloat((isEmpty) ? 0.0 : 1.0)
        let activityAlpha = CGFloat((isEmpty && isDownloading) ? 1.0 : 0.0)
        let inset = self.tableInsetBasedOnState()
        
        /* views */
        let tableView = self.tableView!
        let activityIndicator = self.activityIndicator!
        let pullIndicator = self.pullToRefreshIndicator!
        
        if (isDownloading) {
            activityIndicator.startAnimating()
            pullIndicator.startAnimating()
        }
        
        tableView.contentInset = inset
        
        
        let animBlock = { () -> Void in
            tableView.alpha = tableAlpha
            activityIndicator.alpha = activityAlpha
        }
        let compBlock = { (isFinish:Bool) -> Void in
            if (isDownloading == false) {
                activityIndicator.stopAnimating()
                pullIndicator.stopAnimating()
            }
        }
        
        if (animated) {
            UIView.animate(withDuration: 0.3, delay: 0.0, options: [], animations:animBlock, completion: compBlock)
        } else {
            animBlock()
            compBlock(true)
        }
    }
    
    func tableInsetBasedOnState() -> UIEdgeInsets {
        guard var inset = self.tableView?.contentInset else {
            return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        }
        
        let pullHeight = self.pullToRefreshHeight()
        let topOffset = self.topDefaultOffset()
        
        inset.top = (self.isDownloading) ? (pullHeight + topOffset) : topOffset
        return inset
    }
    
    // MARK:- UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = scrollView.contentOffset.y
        
        self.updatePullIndicator(offset: y)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let y = scrollView.contentOffset.y
        let correctY = self.pullOffsetByScrollOffset(y)
        let pullHeight = self.pullToRefreshHeight()
        
        if (correctY > pullHeight) {
            self.viewModel?.startDownloading()
        }
    }
    
    /**
        Method update frame Y appereance of Pull To Refresh indicator
     */
    func updatePullIndicator(offset y:CGFloat) {
        guard let view = self.pullToRefreshIndicator else {
            return
        }
        
        let pullHeight = self.pullToRefreshHeight()
        var correctY = self.pullOffsetByScrollOffset(y)
        if (correctY < 0) { correctY = 0.0 }
        if (correctY > pullHeight) { correctY = pullHeight }
        
        let indicatorHeight = self.pullToRefreshIndicator.frame.size.height
        let offset = (pullHeight - indicatorHeight) / 2.0
        let x = (self.view.frame.size.width - view.frame.size.width) / 2.0
        let y = correctY + offset
        
        view.setFrameOrigin(x: x, y: y)
    }
    
    /**
        Method calculate correct offset [0 ... max] for scrolling
     */
    func pullOffsetByScrollOffset(_ y:CGFloat) -> CGFloat {
        let topOffset = self.topDefaultOffset()
        return -y - topOffset
    }
    
    /**
        Return Height of topLayoutGuide
     */
    func topDefaultOffset() -> CGFloat {
        return self.topLayoutGuide.length
    }
    
    /**
        Method return the Height of available table offset for Pull To Refresh
     */
    func pullToRefreshHeight() -> CGFloat {
        return 64.0
    }


}
