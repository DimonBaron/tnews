//
//  NewsListViewController+Configure.swift
//  tnews
//
//  Created by Dmitry Avvakumov on 01.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import UIKit

/**
 Extension for configuring UI elements
 
 */

extension NewsListViewController: UITableViewDelegate, UITableViewDataSource {

    func configureUI() {
        self.configureTableView()
    }
    
    func configureTableView() {
        guard let tableView = self.tableView else {
            return
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        
        NewsListTableViewCell.registerCell(tableView: tableView)
    }
    
    // MARK: Model manupulation
    
    func modelAt(indexPath:IndexPath) -> NewsListTableModelProtocol? {
        guard let dataItems = self.dataItems else {
            return nil
        }
        
        let count = dataItems.count
        let index = indexPath.row
        if (index >= count) {
            return nil
        }
        
        return dataItems[index]
    }
    
    // MARK: UITableViewDelegate, UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let dataItems = self.dataItems else {
            return 0
        }
        
        return dataItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let model = self.modelAt(indexPath: indexPath) else {
            return 44.0
        }
        
        return model.estimatedHeight()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let model = self.modelAt(indexPath: indexPath) else {
            return UITableViewCell()
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: model.cellIdentify(), for: indexPath) as! NewsListTableCellProtocol
        cell.configure(model: model)
        
        return cell as! UITableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let model = self.modelAt(indexPath: indexPath) else {
            return
        }
        
        guard let itemID = model.itemID else {
            return
        }
        
        self.router?.openNews(itemID)
    }
    
}
