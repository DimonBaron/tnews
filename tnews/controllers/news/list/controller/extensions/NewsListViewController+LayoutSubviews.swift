//
//  NewsListViewController+LayoutSubviews.swift
//  tnews
//
//  Created by Dmitry Avvakumov on 08.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import UIKit

/**
 Extension for manual manipulating of view hierarchy of StartupViewController
 
 */

extension NewsListViewController {
    
    /** Perform layout */
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.layoutSubviews_layoutPullToRefresh()
    }
    
    func layoutSubviews_layoutPullToRefresh() {
        guard let view = self.pullToRefreshIndicator else {
            return
        }
        
        /* update frames */
        guard let rootView = view.superview else {
            return
        }
        var frame = view.frame
        frame.origin.x = (rootView.frame.size.width - view.frame.size.width) / 2.0
        view.frame = frame
    }
    
    
}
