//
//  NewsListViewController+LoadView.swift
//  tnews
//
//  Created by Dmitry Avvakumov on 01.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import UIKit

/**
 Extension for creation of view hierarchy of StartupViewController
 
 */

extension NewsListViewController {
    
    /** Perform view loading */
    override func loadView() {
        let frame = CGRect.init(x: 0, y: 0, width: 320, height: 480)
        let view = UIView.init(frame: frame)
        view.backgroundColor = UIColor.white
        self.view = view
        
        self.loadView_createTableView()
        self.loadView_createActivityIndicator()
        self.loadView_createPullToRefreshIndicator()
    }
    
    /** Create and customize tableView */
    func loadView_createTableView() {
        /* hierarchy */
        guard let rootView = self.view else {
            return
        }
        
        /* params */
        var frame = CGRect.zero
        frame.size = rootView.frame.size
        
        /* view */
        let view = UITableView.init(frame: frame)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.backgroundColor = UIColor.clear
        
        /* add to scene */
        rootView.addSubview(view)
        
        /* store */
        self.tableView = view
    }
    
    /** Create and customize activity */
    func loadView_createActivityIndicator() {
        /* hierarchy */
        guard let rootView = self.view else {
            return
        }
        
        /* view */
        let view = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
        view.center = rootView.center
        view.autoresizingMask = [.flexibleTopMargin, .flexibleLeftMargin, .flexibleRightMargin, .flexibleBottomMargin]
        view.backgroundColor = UIColor.clear
        
        /* add to scene */
        rootView.addSubview(view)
        
        /* store */
        self.activityIndicator = view
    }
    
    /** Create and customize pull to refresh */
    func loadView_createPullToRefreshIndicator() {
        /* hierarchy */
        guard let rootView = self.view else {
            return
        }
        
        /* view */
        let view = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear
        view.hidesWhenStopped = false
        
        /* align view */
        var frame = CGRect.zero
        frame.size = view.frame.size
        frame.origin.x = (rootView.frame.size.width - view.frame.size.width) / 2.0
        frame.origin.y = 30.0
        view.frame = frame
        
        /* add to scene */
        rootView.addSubview(view)
        
        /* store */
        self.pullToRefreshIndicator = view
    }
    
}

