//
//  NewsListTableViewCell.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 05.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import UIKit

class NewsListTableViewCell: UITableViewCell, NewsListTableCellProtocol {

    /* Outlets */
    @IBOutlet var pictureImageView:UIImageView?
    @IBOutlet var titleLabel:UILabel?
    @IBOutlet var dateLabel:UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        /** Bind default image view */
        if (self.pictureImageView != nil) {
            let imageView = self.pictureImageView!
            let size = imageView.frame.size
            let image = StyleKit.imageOfLogoPicture(imageSize: size)
            imageView.image = image
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: NewsListTableCellProtocol
    
    class func registerCell(tableView:UITableView) {
        let classNameWithType = String(describing: type(of: self) )
        let className = classNameWithType.components(separatedBy: ".").first
        let nib = UINib.init(nibName: className!, bundle: nil)
        let identify = NewsListTableViewModel.init().cellIdentify()
        tableView .register(nib, forCellReuseIdentifier: identify)
    }
    
    func configure(model:NewsListTableModelProtocol) {
        if (model is NewsListTableViewModel) == false { return }
        let castModel = model as! NewsListTableViewModel
        
        self.titleLabel?.text = castModel.title
        self.dateLabel?.text = castModel.dateString
    }
    
}
