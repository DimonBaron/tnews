//
//  NewsListTableViewModel.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 05.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import UIKit

class NewsListTableViewModel: NewsListTableModelProtocol {
    
    // MARK: Model properties
    
    var itemID:NewsFeedPKType?
    
    var title:String?
    var dateString:String?
    
    
    // MARK:- NewsListTableModelProtocol implementation
    
    func cellIdentify() -> String {
        return "NewsItem"
    }
    
    func estimatedHeight() -> CGFloat {
        return 44.0
    }
}

// MARK:- Parse from core data

extension NewsListTableViewModel {
    
    class func modelsFromCoreData(items:[NewsFeed]) -> [NewsListTableViewModel] {
        var output = Array.init() as [NewsListTableViewModel]
        
        for item in items {
            let model = self.modelFromCoreData(item: item)
            output.append(model)
        }
        
        return output
    }
    
    class func modelFromCoreData(item:NewsFeed) -> NewsListTableViewModel {
        let model = NewsListTableViewModel()
        
        model.itemID = item.itemID?.intValue
        model.title = item.text
        model.dateString = item.publishDatePrinable()
        
        return model
    }
    
}
