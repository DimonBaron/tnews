//
//  NewsListTableProtocols.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 05.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import UIKit

protocol NewsListTableModelProtocol {
    
    /** 
        Method return cell identify of associated cell  
     */
    func cellIdentify() -> String
    
    /**
        Estimated height of associated cell
     */
    func estimatedHeight() -> CGFloat
    
    var itemID:NewsFeedPKType? { get }
    
}

protocol NewsListTableCellProtocol {
    
    static func registerCell(tableView:UITableView)
    func configure(model:NewsListTableModelProtocol)
    
}
