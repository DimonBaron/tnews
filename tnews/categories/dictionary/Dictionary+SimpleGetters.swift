//
//  Dictionary+SimpleGetters.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 07.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation

extension Dictionary {
    
    public func integerForKey(_ key:String) -> Int {
        let dict:Dictionary<String, Any> = self as! Dictionary<String, Any>
        
        let rawValue = dict[key]
        if (rawValue == nil) { return 0 }
        
        if rawValue is Int { return rawValue as! Int }
        
        if rawValue is String {
            return Int( rawValue as! String )!
        }
        
        return 0
    }
    
    public func stringForKey(_ key:String) -> String {
        let dict:Dictionary<String, Any> = self as! Dictionary<String, Any>
        
        let rawValue = dict[key]
        if (rawValue == nil) { return "" }
        
        if rawValue is String { return rawValue as! String }
        
        if rawValue is Int {
            return String( rawValue as! Int )
        }
        
        return ""
    }
    
}
