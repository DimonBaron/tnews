//
//  String+TextSize.swift
//  tnews
//
//  Created by Dmitry Avvakumov on 28.07.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import UIKit

/**
    Extension for string bounds calculation
 */

extension String {
    
    /**
        Method perform calculating of bounds of string by providing font for unlimited width
            
        - parameter font: Provided UIFont
     
        - returns: CGSize of bounds
     */
    func textSize(font:UIFont) -> CGSize! {
        return self.textSize(font: font, width: 0.0)
    }
    
    /**
     Method perform calculating of bounds of string by providing font and width
     
     - parameter font: Provided UIFont
     - parameter width: Provided width
     
     - returns: CGSize of bounds
     */
    func textSize(font:UIFont, width:Double) -> CGSize! {
        let attrs = [ NSFontAttributeName: font ]
        
        let size:CGSize
        if (width == 0.0) {
            size = (self as NSString).size(attributes: attrs)
        } else {
            let availableSize = CGSize(width: Double(width), height: Double.greatestFiniteMagnitude)
            
            let rect = (self as NSString).boundingRect(with: availableSize, options: .usesLineFragmentOrigin, attributes: attrs, context: nil)
            size = rect.size
        }
        let w = ceil(size.width)
        let h = ceil(size.height)
        
        return CGSize.init(width: w, height: h)
    }
    
}
