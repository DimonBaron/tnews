//
//  NSObject+RuntimeTypes.swift
//  tnews
//
//  Created by Dmitry Avvakumov on 07.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation

enum PropertyTypes:String {
    case OptionalInt = "Optional<Int>"
    case Int = "Int"
    case OptionalString = "Optional<String>"
    case String = "String"
}

extension NSObject {
    /** 
        Returns the property type
    */
    func getTypeOfProperty(name:String) -> String? {
        let type: Mirror = Mirror(reflecting:self)
        
        for child in type.children {
            if child.label! == name {
                return String(describing: type(of: child.value))
            }
        }
        return nil
    }
    
    /**
        Property Type Comparison
     */
    func propertyIsOfType(propertyName:String, type:PropertyTypes)->Bool {
        if getTypeOfProperty(name: propertyName) == type.rawValue {
            return true
        }
        
        return false
    }
}
