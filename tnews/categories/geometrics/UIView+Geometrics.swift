//
//  UIView+Geometrics.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 28.07.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func setFrame(origin:CGPoint) {
        var frame = self.frame
        frame.origin = origin
        self.frame = frame
    }
    
    func setFrameOrigin(x:CGFloat, y:CGFloat) {
        var frame = self.frame
        frame.origin.x = x
        frame.origin.y = y
        self.frame = frame
    }
    
    func setFrameOrigin(x:CGFloat) {
        var frame = self.frame
        frame.origin.x = x
        self.frame = frame
    }
    
    func setFrameOrigin(y:CGFloat) {
        var frame = self.frame
        frame.origin.y = y
        self.frame = frame
    }
    
    func alignToCenterSuperview() {
        guard let rootView = self.superview else {
            return
        }
        
        let view = self
        
        let rootFrame = rootView.frame
        let x = rootFrame.size.width / 2.0
        let y = rootFrame.size.height / 2.0
        let center = CGPoint(x: x, y: y)
        view.center = center
    }
    
    func setAnchorPointNew(anchorPoint: CGPoint) {
        let view = self
        
        var newPoint = CGPoint(x:view.bounds.size.width * anchorPoint.x, y:view.bounds.size.height * anchorPoint.y)
        var oldPoint = CGPoint(x:view.bounds.size.width * view.layer.anchorPoint.x, y:view.bounds.size.height * view.layer.anchorPoint.y)
        
        newPoint = newPoint.applying(view.transform)
        newPoint = newPoint.applying(view.transform)
        oldPoint = oldPoint.applying(view.transform)
        
        var position : CGPoint = view.layer.position
        
        position.x -= oldPoint.x
        position.x += newPoint.x;
        
        position.y -= oldPoint.y;
        position.y += newPoint.y;
        
        view.layer.position = position;
        view.layer.anchorPoint = anchorPoint;
    }

    
}
