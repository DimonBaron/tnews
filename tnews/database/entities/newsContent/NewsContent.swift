//
//  NewsContent.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 07.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import UIKit
import CoreData

class NewsContent: NSManagedObject, CoreDataServiceImport {

    // MARK:- CoreDataServiceImport
    static var jsonMap = [
        "itemID": "title.id",
        "fullText": "content",
    ]
    
    static var primaryKey: String = "itemID"
    
    func didImport(_ dict: Dictionary<String, Any>) {
        /* relation */
        var newsFeed:NewsFeed? = nil
        
        /* date */
        if let titleFeed = dict["title"] as? Dictionary<String, Any> {
            if let itemID = titleFeed["id"] as? String {
                self.itemID = NSNumber(value: Int.init(itemID)!)
            }
            
            /* relation */
            let context = self.managedObjectContext
            newsFeed = NewsFeed.importObject(json: titleFeed, in: context!)
        }
        
        if let text = dict["content"] as? String {
            self.fullText = text
        }
        
        /* assign */
        self.newsFeed = newsFeed
    }
    
}
