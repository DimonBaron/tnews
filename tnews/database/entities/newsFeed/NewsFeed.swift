//
//  NewsFeed.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 06.08.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import UIKit
import CoreData

typealias NewsFeedPKType = Int

class NewsFeed: NSManagedObject, CoreDataServiceImport {
    
    // MARK:- CoreDataServiceImport
    static var jsonMap = [
        "itemID": "id",
        "name": "name",
        "text": "text",
        "publishDate": "publicationDate.milliseconds"
    ]
    
    static var primaryKey: String = "itemID"
    
    func didImport(_ dict: Dictionary<String, Any>) {
        if let itemID = dict["id"] as? String {
            self.itemID = NSNumber(value: Int(itemID)!)
        }
        if let name = dict["name"] as? String {
            self.name = name
        }
        if let text = dict["text"] as? String {
            self.text = text.decodeHTML()
        }
        
        /* date */
        if let publicationDate = dict["publicationDate"] as? Dictionary<String, Any> {
            if let mill = publicationDate["milliseconds"] as? Int {
                let time = Double( mill ) / 1000.0
                let date = Date(timeIntervalSince1970: time)
                
                self.publishDate = date as NSDate
            }
        }
    }
    
    // MARK: - Custom properties
    public func publishDatePrinable() -> String {
        if (self.publishDate == nil) {
            return "-"
        }
        let date = self.publishDate! as Date
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM YYYY, HH:mm"
        formatter.locale = Locale.init(identifier: "ru")
        formatter.timeZone = TimeZone.current
        
        return formatter.string(from: date)
    }
    
}
