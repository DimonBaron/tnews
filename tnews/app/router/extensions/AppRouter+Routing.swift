//
//  AppRouter+Routing.swift
//  tnews
//
//  Created by Dmitry Avvakumov on 28.07.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import UIKit

extension AppRouter {
    
    public func presentStartupController(animated:Bool) {
        let controller = StartupViewController()
        
        self.changeRoot(viewController: controller, animated: animated)
    }
    
    public func presentMainController(animated:Bool) {
        let controller = NewsListAssembly.assembly()
        let navController = UINavigationController.init(rootViewController: controller)
        
        self.changeRoot(viewController: navController, animated: animated)
    }
    
    /**
        Method perform animated changing of root view controller by application window
     
     */
    private func changeRoot(viewController:UIViewController, animated:Bool) {
        guard let window = self.window else {
            return
        }
        
        /* variable for determinate need animation or not */
        var needAnimation = animated
        
        /* for `initial` state animation not needed */
        if (window.rootViewController == nil) {
            needAnimation = false
        }
        
        /* if animation not needed */
        if (needAnimation == false) {
            window.rootViewController = viewController
            return
        }
        
        /* other case */
        let snapShot = window.snapshotView(afterScreenUpdates: true)
        viewController.view.addSubview(snapShot!)
        window.rootViewController = viewController
        
        UIView.animate(withDuration: 0.3, animations: {
            snapShot?.layer.opacity = 0;
            snapShot?.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5);
        }, completion: { (finished) in
            snapShot?.removeFromSuperview()
        })
    }
    
}
