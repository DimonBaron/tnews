//
//  AppRouter.swift
//  tnews
//
//  Created by Avvakumov Dmitry on 27.07.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import Foundation
import UIKit

/**
    Class perform routing between common application states
 
    That is `states` - It`s global ended situations:
    - Startup animation
    - Auth
    - Main screens
 
 */
class AppRouter {
    
    /** Shared instance */
    static let sharedInstance = AppRouter()
    
    var window:UIWindow? = nil
    
    /**
        Router enter point
     
     */
    public func drive(window:UIWindow, animated:Bool) {
        self.window = window
        self.showedState = .Unknow
        
        self.updateAppState(animated: animated)
    }
    
    /**
        Enum for common states
        
        ## Types
        
        - Startup - Just after startup application for presenting animation
        - Main - Main state
     */
    enum State:Int {
        case Unknow
        case Startup
        case Main
    }
    
    /** Internal state variable that determinate current showed routing state */
    private var showedState:State = .Unknow
    
    /**
        Method return state of application match to current situation
     */
    private func appMatchState() -> State {
        if (self.startupIsShowed == false) {
            return .Startup
        }
        
        return .Main
    }
    
    /** Metrics of current routing */
    private var startupIsShowed = false
    
    /** Methods for manipilation metrics */
    public func setStartup(showed:Bool, animated:Bool) {
        startupIsShowed = showed
        
        self.updateAppState(animated: animated)
    }

    /**
        Method for updating state routing of application
     */
    public func updateAppState(animated:Bool) {
        let state = self.appMatchState()
        
        /* if `showed` state is equal to `match` state do notning */
        if (state == showedState) { return }
        
        self.performInternalUpdate(state: state, animated: animated)
    }
    
    private func performInternalUpdate(state:State, animated:Bool) {
        switch state {
        case .Unknow:
            break
        case .Startup:
            self.presentStartupController(animated: animated)
            break
        case .Main:
            self.presentMainController(animated: animated)
            break
        }
        
        self.showedState = state
    }
    
    
    
    
    
}
