//
//  StringTextSizeTest.swift
//  tnews
//
//  Created by Dmitry Avvakumov on 28.07.17.
//  Copyright © 2017 Dmitry Avvakumov. All rights reserved.
//

import XCTest

class StringTextSizeTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSizeForSingleLineStringWithSystemFont() {
        let text = "Welcome to our application!"
        let font = UIFont.systemFont(ofSize: 16.0)
        
        let size = text.textSize(font: font)
        let expectedSize = CGSize(width: 203.0, height: 20.0)
        
        XCTAssertTrue(__CGSizeEqualToSize(size!, expectedSize), "Actual size \(String(describing: size)), expected: \(String(describing: expectedSize))")
    }
    
    func testSizeForWidthWithSystemFont() {
        let text = "Welcome to our application!"
        let font = UIFont.systemFont(ofSize: 16.0)
        let width = 100.0
        
        let size = text.textSize(font: font, width: width)
        let expectedSize = CGSize(width: 91.0, height: 58.0)
        
        XCTAssertTrue(__CGSizeEqualToSize(size!, expectedSize), "Actual size \(String(describing: size)), expected: \(String(describing: expectedSize))")
    }

    
}
